const getSum = (str1, str2) => {
  if (typeof str1 !== 'string' && typeof str2 !== 'string') return false;

  const isAllNumber = (str) => {
    for (const char of str) {
      if ( Number.isNaN(Number(char)) ) return false;
    }
    return true;
  }
  if (!isAllNumber(str1) && !isAllNumber(str2)) return false;
  
  if (str1 === '') str1 = '0';
  if (str2 === '') str2 = '0';

  let bigerString = str1.length > str2.length ? str1.length : str2.length;
 
  let resultStr = '';
  let inMemory = 0;

  for (let i = bigerString - 1; i >= 0; i--) {
    if (str1[i] === undefined) {
      resultStr = str2[i] + resultStr;
      continue;
    }

    if (str2[i] === undefined) {
      resultStr = str1[i] + resultStr;
      continue;
    }
    let temp = Number(str1[i]) + Number(str2[i]) + inMemory;
    inMemory = 0;
    if (temp > 9) {
      temp = temp % 10;
      inMemory = 1;
    }

    resultStr = temp + resultStr;
  }
  if (inMemory) resultStr = '1' + resultStr;

  return resultStr;
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let postCounter = 0;
  let commenCountert = 0;

  for (const post of listOfPosts) {
    if (post.author === authorName) postCounter++;
    if ('comments' in post) {
      for (const comment of post.comments) {
        if (comment.author === authorName) commenCountert++;
      }
    }
  }

  return `Post:${postCounter},comments:${commenCountert}`;
};

const tickets=(people)=> {
  let chash25 = 0;
  let chash50 = 0;
  for (const person of people) {
    switch (person.toString()) {
      case '25':
        chash25++;
        break;
      case '50':
        if (chash25 < 1) return 'NO';
        chash25--;
        chash50++;
        break;
      case '100':
        if (chash25 > 0 && chash50 > 0) {
          chash25--;
          chash50--;
        } else if (chash25 > 2) {
          chash25 -= 3;
        } else {
          return 'NO';
        }
        break;
      default:
        return 'NO';
    }
  }
  return 'YES';
};

module.exports = {getSum, getQuantityPostsByAuthor, tickets};
